﻿// 
//  DetectSentence.cs for Project projet voltaire 
// 
//  Made by Julien DOCHE
//  Login doche_j 
//  Email doche_j@epita.fr
// 
//  Started on 30/06/2015 at 18:55 
//  Last update 11/07/2015 at 22:22 
// 

using System;

namespace Project_projet_voltaire
{
    internal class DetectSentence
    {
        ///// <summary>
        /////   récupère le fichier html de la page et renvoie la pharse contenue dans celle-ci
        ///// </summary>
        ///// <param name="html"></param>
        ///// <returns></returns>
        //public static string[] Alternate_2_GetSentence(string html)
        //{
        //    HtmlDocument h = null;
        //        //  il faut trouver un moyen de convertir une string en htmldocument

        //    HtmlElementCollection elts = h.GetElementsByTagName("span");

        //    string[] result = new string[elts.Count];

        //    for (int i = 0; i < elts.Count; i++)
        //        result[i] = elts[i].InnerText;
        //    //example : Que j'intervienne en premier ou en second à la réunion, cela m'est égal..html
        //    return result;
        //}

        ///// <summary>
        /////   récupère le fichier html de la page et renvoie la pharse contenue dans celle-ci
        /////   cette version pourrait utiliser HTMLAgilityPack
        ///// </summary>
        ///// <param name="html"></param>
        ///// <returns></returns>
        //public static string[] Alternate_GetSentence(string html)
        //{
        //    string[] result = new string[0];


        //    //example : Que j'intervienne en premier ou en second à la réunion, cela m'est égal..html
        //    return result;
        //}

        public static string[] GetSentence(string html)
        {
            const string detect =
                "<span class=\"pointAndClickSpan\" style=\"white-space: normal; \" role=\"presentation\">";

            string[] res = html.Split(
                                      new[] {detect},
                                      StringSplitOptions.None);
            if (res.Length < 3)
                return new string[0];

            string[] result = new string[res.Length - 2];

            for (int j = 0; j < result.Length; j++)
                result[j] = res[j + 1].Substring(0, res[j + 1].Length - 7);

            return result;
        }
    }
}
