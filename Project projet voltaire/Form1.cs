﻿// 
//  Form1.cs for Project projet voltaire 
// 
//  Made by Julien DOCHE
//  Login doche_j 
//  Email doche_j@epita.fr
// 
//  Started on 30/06/2015 at 18:55 
//  Last update 11/07/2015 at 22:09 
// 

using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Project_projet_voltaire.Properties;

namespace Project_projet_voltaire
{
    public partial class Form1 : Form
    {
        public Form1() { InitializeComponent(); }

        //private void Awesomium_Windows_Forms_WebControl_ShowCreatedWebView(
        //    object sender,
        //    ShowCreatedWebViewEventArgs e)
        //{

        //    JSValue ele = Navigateur.ExecuteJavascriptWithResult(@"document.elementFromPoint(" + e.Specs.InitialPosition.X + "," + e.Specs.InitialPosition.Y + ").outerHTML");
        //    string userinput = ele.ToString();
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            Navigateur.Source = new Uri("http://projet-voltaire.fr");
        }

        private void button2_Click(object sender, EventArgs e)
        {
                Navigateur.SelectAll();


                string[] sentence = DetectSentence.GetSentence(Navigateur.Selection.HTML);


                if (!sentence.Any())
                    textBox1.Text = Resources.Form1_button2_Click_pas_de_phrase_trouvée;
                else
                {
                    textBox1.Text = "";
                    foreach (string t in sentence)
                        textBox1.Text += t;
                }
                LocalDatabase.AddToUnknownBase(sentence);
                //sentence[GetAnswer.Answer(sentence)];
            
        }
    }
}
