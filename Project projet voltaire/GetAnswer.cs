﻿// 
//  GetAnswer.cs for Project projet voltaire 
// 
//  Made by Julien DOCHE
//  Login doche_j 
//  Email doche_j@epita.fr
// 
//  Started on 30/06/2015 at 18:55 
//  Last update 10/07/2015 at 22:25 
// 

namespace Project_projet_voltaire
{
    internal class GetAnswer
    {
        /// <summary>
        /// Récupere la position de la réponce dans la phrase.
        /// on regarde d'abord dans la database serveur puis dans la database locale.
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns></returns>
        public static int Answer(string[] sentence)
        {
            int serverAnswer = ServerDatabase.Get(sentence);
            if (serverAnswer != -1)
            {
                LocalDatabase.Add(sentence, serverAnswer); // propriété du add : il n'y a pas de duplicates
                return serverAnswer;
            }

            int localAnswer = LocalDatabase.Get(sentence);
            if (localAnswer != -1)
                return localAnswer;
            // ? wut ? soit cest une erreur, soit jai pas compris ce que cette fonction est censee faire : pls make a summary for this method
            // Update : C'était bien une erreur !
            LocalDatabase.AddToUnknownBase(sentence);
            return -1;
        }


        // Notes de Phaco : a mon sens Answer(string sentence) est problematique
        // Comme la verification server est faite avant la verif locale, cela peut mener a des duplicats dans la database locale ce qui nuirait grandement a l'optimisation et l'efficacite du programme
        // je propose donc cette version

        // UPDATE : oui mais si une réponce incorrecte est enregistrée en local il n'y a aucun moyen de la changer

        //public static string Proposition_Answer(string sentence)
        //{
        //    string localAnswer = LocalDatabase.Get(sentence);
        //    if (localAnswer != "")
        //        return localAnswer;
        //    string serverAnswer = ServerDatabase.Get(sentence);
        //    if (serverAnswer != "")
        //    {
        //        LocalDatabase.Add(sentence, serverAnswer);
        //        return serverAnswer;
        //    }

        //    LocalDatabase.AddToUnknownBase(sentence);
        //    return "no answer available";
        //}
    }
}
