﻿// 
//  LocalDatabase.cs for Project projet voltaire 
// 
//  Made by Julien DOCHE
//  Login doche_j 
//  Email doche_j@epita.fr
// 
//  Started on 30/06/2015 at 18:55 
//  Last update 30/08/2015 at 10:12 
// 

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Project_projet_voltaire.Properties;

namespace Project_projet_voltaire
{
    internal class LocalDatabase
    {
        /// <summary>
        ///   self explanatory
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns></returns>
        public static int Get(string[] sentence)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///   self explanatory
        /// </summary>
        /// <param name="sentence"></param>
        /// <param name="answer"></param>
        public static void Add(string[] sentence, int answer)
        {
            Remove(sentence);
            // on commence par retirer la réponce potentiellement incorrecte
            RemoveFromUnknownBase(sentence);


            throw new NotImplementedException();
        }

        /// <summary>
        ///   self explanatory
        /// </summary>
        /// <param name="sentence"></param>
        public static void Remove(string[] sentence)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///   self explanatory
        /// </summary>
        /// <param name="sentence"></param>
        public static void AddToUnknownBase(string[] sentence)
        {
            XmlDocument sentenceXml = new XmlDocument();

            List<XElement> zd = sentence.Select(s => new XElement("clickable",s))
                                        .ToList();

            XDocument doc = new XDocument(zd);


                // trouver un moyen de convertir string[] en XmlDocument
            for (int i = 0; i < sentence.GetLength(0); i++)
            {
                sentenceXml.CreateElement("clickable",sentence[i]);
            }

            SqlConnection cn =
                new SqlConnection(
                    Settings.Default.LocalUnknownConnectionString);
            try
            {
                string sqlcommand =
                    "INSERT INTO UnknownAnswers (id,Sentence) values("
                    + new Random().Next(1024) + "," + sentenceXml.OuterXml + ")";
                SqlCommand exeSql = new SqlCommand(sqlcommand, cn);
                cn.Open();
                exeSql.ExecuteNonQuery();
            }
            finally
            {
                cn.Close();
            }

            throw new NotImplementedException();
        }

        /// <summary>
        ///   self explanatory
        /// </summary>
        /// <param name="sentence"></param>
        public static void RemoveFromUnknownBase(string[] sentence)
        {
            throw new NotImplementedException();
        }
    }
}
