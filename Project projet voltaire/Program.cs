﻿// 
//  Program.cs for Project projet voltaire 
// 
//  Made by Julien DOCHE
//  Login doche_j 
//  Email doche_j@epita.fr
// 
//  Started on 20/06/2015 at 14:51 
//  Last update 20/06/2015 at 16:55 
// 

using System;
using System.Windows.Forms;

namespace Project_projet_voltaire
{
    internal static class Program
    {
        /// <summary>
        ///   The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
